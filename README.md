# Custom Exchangers
> Custom Exchangers by CoolHwip using QBCore  
> QBCore : https://github.com/qbcore-framework

## Overview
---
> This is a custom script/resource for fivem that allows server owners to create `exchangers` dynamically  
> 
> An `exchanger` is an entity/npc that allows players to exchange one type of item for another
>
> Note : this is copied over from my private repo that I use for my dev server, hence the lack of commits.


## Depdencies
---
> Usage of QBCore for player data and inventory management
- QBCore
- QB-Inventory

## Usage
---
> Feel free to use this and make modifications for your server, leave credit if you'd like.


## Setup
---
### Database
Create a DB table using the sql statement below or reference it

```sql
CREATE TABLE `exchangers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ped_model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `coordinates` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `receive_item` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `receive_amount` bigint NOT NULL,
  `give_item` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `give_amount` bigint NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) 
```
### DB Entry
> Here is an example of a db entry
```sql
INSERT INTO exchangers
(ped_model, name, coordinates, receive_item, receive_amount, give_item, give_amount)
VALUES('csb_prologuedriver', 'Enrique (Laptop Seller)', '{
            "x" : 96.00,
            "y" : -23.36,
            "z" : 67.54,
            "h" : 210.22
        }', 'cash', 1, 'laptop', 10);
```

### Values
> Explanation for the values used in entries
- `ped_model`: Model for the npc (ped models https://docs.fivem.net/docs/game-references/ped-models/)
- `name`: The string that will be displayed at coordinates
- `coordinates`: Coordinates for the exchanger, which will create a temoporary invincible npc, a circle marker, and 3d text. ( x,y,z,h (h = heading) )
> Valid items can be found in `qb-core/shared/items.lua` when using qb-core
- `receive_item`: Item to receive from players. (should be valid item for your server.)
- `receive_amount`: Amount to receive from players.
- `give_item`: Item to give to player.
- `give_amount`: Amount to give to player


### `config.js`
> The npc/marker/3dtext are intended to appear when a player is near the coordinates.
> The distances can be configured here.
```javascript
const resource_config = {
    "text_draw_distance" : 10, // how close before text appears
    "npc_spawn_distance": 30, // how close before npc spawns in
    "keypress_distance" : 2, // how close player needs to be to trigger action via hotkey
    "marker_distance" : 10, // how close before marker shows up
}
```

## References
---
- `QBCore Documentation`: https://qbcore-framework.github.io/qb-docs/
- `Fivem Docs`: https://docs.fivem.net/docs/


