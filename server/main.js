let QBCore = exports['qb-core']['GetCoreObject']()

// events and handlers
// take item from player inv
RegisterNetEvent("hwip-custom-exchangers:server:removeItem")
onNet("hwip-custom-exchangers:server:removeItem", (source, item, amount) => {
    let Player = QBCore.Functions.GetPlayer(source)
    Player.Functions.RemoveItem(item, amount)
})

// add item to player inv
RegisterNetEvent("hwip-custom-exchangers:server:addItem")
onNet("hwip-custom-exchangers:server:addItem", (source, item, amount) => {
    let Player = QBCore.Functions.GetPlayer(source)
    Player.Functions.AddItem(item, amount)
})


// give money to player
RegisterNetEvent("hwip-custom-exchangers:server:payout")
onNet("hwip-custom-exchangers:server:payout", (source, amount) => {
    let Player = QBCore.Functions.GetPlayer(source)
    Player.Functions.AddMoney("cash", amount)
})

// take money from player
RegisterNetEvent("hwip-custom-exchangers:server:payment")
onNet("hwip-custom-exchangers:server:payment", (source, amount) => {
    let Player = QBCore.Functions.GetPlayer(source)
    Player.Functions.RemoveMoney("cash", amount)
})

// callback to return player cash
QBCore.Functions.CreateCallback('hwip-custom-exchangers:GetCash', (source, cb) => {
    let player = QBCore.Functions.GetPlayer(source)
    if (player.PlayerData.money.cash) {
        cb(player.PlayerData.money.cash)
    }
})


// callback to return player total weight
QBCore.Functions.CreateCallback('hwip-custom-exchangers:getTotalWeight', (source, cb, items) => {
    let totalWeight = QBCore.Player.GetTotalWeight(items)
    cb(totalWeight)
})

// init spawning all the exchangers
const init = async (source) => {
    const exchangerList = await exports.oxmysql.query_async('SELECT * FROM exchangers', [])
    exchangerList.forEach(exchanger => {
        exchanger.coordinates = JSON.parse(exchanger.coordinates)
    })
    emitNet("hwip-custom-exchangers:exchangers", source, exchangerList)
}

// allow client to init
RegisterNetEvent("hwip-custom-exchangers:server:init")
onNet("hwip-custom-exchangers:server:init", (source) => {
    init(source)
})
