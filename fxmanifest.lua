fx_version 'cerulean'
game 'gta5'

author 'coolhwip'
description 'Custom Exchangers by CoolHwip using QBCore'
version '1.0.0'

client_scripts {
	'client/main.js',
    'client/exchanger.js',
}
server_scripts {
    'server/main.js',
}

dependencies {
	'qb-core',
    'qb-inventory',
}

shared_scripts {
    'config.js',
}

