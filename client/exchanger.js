const createExchanger = (exchangerInfo) => {
    const ped = PlayerPedId()

    // spawn client side npc
    createTempNPC(ped, exchangerInfo.coordinates, exchangerInfo.ped_model)

    // 3d text on npc
    create3DText(ped, exchangerInfo.coordinates, exchangerInfo.name)

    // draw circle on npc
    drawCircleMarker(ped, exchangerInfo.coordinates)

    // action on keypress within area
    keyPressAction(ped, exchangerInfo.coordinates, () => {
        exchangeWithPlayer(exchangerInfo.receive_item, exchangerInfo.give_item, exchangerInfo.receive_amount, exchangerInfo.give_amount,)

    })

}

const getItemAmount = (itemsList, itemName) => {
    let itemAmount = 0
    itemsList.forEach(item => {
        if (itemName === item.name) {
            itemAmount = item.amount
        }
    })
    return itemAmount
}

const exchangeWithPlayer = async (receive_item, give_item, receive_amount, give_amount) => {

    const hasItem = await QBCore.Functions.HasItem(receive_item)

    if (hasItem) {
        const playerData = QBCore.Functions.GetPlayerData()

        // use payout flag to determine if transaction can occur
        let payout = false
        let error_message

        // determine if transaction can occur
        if (receive_item !== "cash") { // check that player has the rquired amount
            const ownedAmount = getItemAmount(Object.values(playerData.items), receive_item)

            if (ownedAmount >= receive_amount) {
                payout = true
            } else {
                error_message = `You dont have enough ${receive_item}, need ${receive_amount}`
            }

        } else { // check that player has enough cash
            let ownedCash
            QBCore.Functions.TriggerCallback('hwip-custom-exchangers:GetCash', (amount) => {
                ownedCash = amount
            })

            // wait for callback to populate cash
            while (! ownedCash) {
                await sleep(500)
            }
            // condition ? exprIfTrue : exprIfFalse
            ownedCash >= receive_amount ? payout = true : error_message = `You don't have enough cash, need ${receive_amount}`

        }

        // check if player can hold item
        if (give_item !== "cash") {
            let totalWeight

            QBCore.Functions.TriggerCallback('hwip-custom-exchangers:getTotalWeight', (weight) => {
                totalWeight = weight
            }, playerData.items)

            while (! totalWeight) {
                await sleep(500)
            }

            const iteminfo = QBCore.Shared.Items[give_item]
            const totalItemWeight = iteminfo.weight * give_amount

            if ((totalItemWeight + totalWeight) <= QBCore.Config.Player.MaxWeight) {
                payout = true
            } else {
                payout = false
                error_message = "Your Inventory is too full."
            }
        }

        if (payout) { 
            
            // handle receive item (take from player)
            if (receive_item === "cash") { 
                // take cash from player
                emitNet("hwip-custom-exchangers:server:payment", playerData.source, receive_amount)
            } else { 
                // take item from player
                emitNet("hwip-custom-exchangers:server:removeItem", playerData.source, receive_item, receive_amount)

            }

            // handle giving item to player
            if (give_item === "cash") { 
                // give cash to player
                emitNet("hwip-custom-exchangers:server:payout", playerData.source, give_amount)
            } else { 
                // give item to player
                emitNet("hwip-custom-exchangers:server:addItem", playerData.source, give_item, give_amount)
            }

            QBCore.Functions.Notify(`Removed ${receive_amount} of ${receive_item}`, "success")
            QBCore.Functions.Notify(`Received ${give_amount} of ${give_item}`, "success")

        } else {
            QBCore.Functions.Notify(error_message, "error")
        }

    } else {
        QBCore.Functions.Notify(`You don't have any ${receive_item} on you.`, "error")
    }
}

// events and handler
RegisterNetEvent("hwip-custom-exchangers:exchangers");
onNet("hwip-custom-exchangers:exchangers", (exchangerList) => { 
    // loop and create exchangers
    exchangerList.forEach(exchanger => {
        createExchanger(exchanger)
    })

})

// listen for player load event
on("QBCore:Client:OnPlayerLoaded", async () => {
    await sleep(10000)
    const playerData = QBCore.Functions.GetPlayerData()
    emitNet("hwip-custom-exchangers:server:init", playerData.source)
})
