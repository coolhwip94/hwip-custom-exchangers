let QBCore = exports['qb-core']['GetCoreObject']()

const sleep = (milliseconds) => { // sleep function
    return new Promise(resolve => setTimeout(resolve, milliseconds))
}

const draw3dText = async (coords, text) => { // coords will need to be object {x: 1, y: 2, z: 3}
    let [onScreen, worldX, worldY] = World3dToScreen2d(coords.x, coords.y, coords.z)
    if (onScreen) {
        SetTextScale(1.0, 0.45)
        SetTextFont(4)
        SetTextColour(255, 255, 255, 255)
        SetTextEdge(2, 0, 0, 0, 150)
        SetTextProportional(1)
        SetTextOutline()
        SetTextCentre(1)
        SetTextEntry("STRING")
        AddTextComponentString(text)
        DrawText(worldX, worldY)
    }
}

const create3DText = async (player_ped, text_coords, text) => {

    let player_coords = GetEntityCoords(player_ped)
    let distance = GetDistanceBetweenCoords(player_coords[0], player_coords[1], player_coords[2], text_coords.x, text_coords.y, text_coords.z)

    // needs to loop forever and continue checking
    while (true) {

        player_coords = GetEntityCoords(player_ped)
        distance = GetDistanceBetweenCoords(player_coords[0], player_coords[1], player_coords[2], text_coords.x, text_coords.y, text_coords.z)
        if (distance < resource_config.text_draw_distance) {
            draw3dText(text_coords, text)
        }

        // 1 ms, basically every frame
        await sleep(1)
    }
}


const spawnInvincibleNPC = async (coords, ped_model) => {

    const hash = GetHashKey(ped_model);
    // request the model and wait for game to load it
    RequestModel(hash);
    while (!HasModelLoaded(hash)) {
        await sleep(500);
    }

    const npc = CreatePed(26, hash, coords.x, coords.y, coords.z, coords.h, false, false)
    SetEntityInvincible(npc, true)
    TaskSetBlockingOfNonTemporaryEvents(npc, true)

    // wait for him to fall on ground
    await sleep(500)
    FreezeEntityPosition(npc, true)

    return npc
}

const createTempNPC = async (player_ped, npc_coords, ped_model) => {

    let spawnedNpc
    let isNpcSpawned = false
    while (true) {
        let player_coords = GetEntityCoords(player_ped)
        let distance = GetDistanceBetweenCoords(player_coords[0], player_coords[1], player_coords[2], npc_coords.x, npc_coords.y, npc_coords.z)

        if (distance < resource_config.npc_spawn_distance && ! isNpcSpawned) {
            spawnedNpc = await spawnInvincibleNPC(npc_coords, ped_model)
            isNpcSpawned = true
        } else if (distance > resource_config.npc_spawn_distance && isNpcSpawned) { // despawn entity
            DeleteEntity(spawnedNpc)
            isNpcSpawned = false
        }

        await sleep(1000)
    }
}


const keyPressAction = async (player_ped, keypress_coords, actionCallback) => {

    while (true) {
        let player_coords = GetEntityCoords(player_ped)
        let distance = GetDistanceBetweenCoords(player_coords[0], player_coords[1], player_coords[2], keypress_coords.x, keypress_coords.y, keypress_coords.z)
        if (IsControlJustReleased(0, 38) && distance < resource_config.keypress_distance) {
            actionCallback()
        }
        await sleep(1)
    }
}

const drawCircleMarker = async (player_ped, marker_coords) => {
    while (true) {
        let player_coords = GetEntityCoords(player_ped)
        let distance = GetDistanceBetweenCoords(player_coords[0], player_coords[1], player_coords[2], marker_coords.x, marker_coords.y, marker_coords.z)
        if (distance < resource_config.marker_distance) {
            DrawMarker(25, marker_coords.x, marker_coords.y, marker_coords.z - 0.5, 0.0, 0.0, 0.0, 0.0, 180.0, 0.0, 2.0, 2.0, 2.0, 6, 165, 28, 0.1, false, true, 2, null, null, false)

        }
        await sleep(1)
    }


}
