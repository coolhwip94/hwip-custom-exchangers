const resource_config = {
    "text_draw_distance" : 10, // how close before text appears
    "npc_spawn_distance": 30, // how close before npc spawns in
    "keypress_distance" : 2, // how close player needs to be to trigger action via hotkey
    "marker_distance" : 10, // how close before marker shows up
}